#! /usr/bin/env python
import time
import rospy
import numpy as np 
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import Float64
from robot_util.msg import Qvec
import string 
from std_msgs.msg import Float64
from std_msgs.msg import Float32

First=True
def callback(data):
    global First
    
    print(data.data)
    print(len(data.data))
    if len(data.data) ==6:
        pub1=rospy.Publisher('/joint_a1_position_controller/command',Float64,queue_size=1)
        pub2=rospy.Publisher('/joint_a2_position_controller/command',Float64,queue_size=1)
        pub3=rospy.Publisher('/joint_a3_position_controller/command',Float64,queue_size=1)
        pub4=rospy.Publisher('/joint_a4_position_controller/command',Float64,queue_size=1)
        pub5=rospy.Publisher('/joint_a5_position_controller/command',Float64,queue_size=1)
        pub6=rospy.Publisher('/joint_a6_position_controller/command',Float64,queue_size=1)
    
    # rospy.init_node('utility_node',anonymous=True)
        qvec=data.data
        i=0
        time.sleep(1)
        if First==True:
        
            pub1.publish(np.float64(0))
            pub2.publish(np.float64(0))
            pub3.publish(np.float64(0))
            pub4.publish(np.float64(0))
            pub5.publish(np.float64(0))
            pub6.publish(np.float64(0))
            First=False
            time.sleep(1)
        
       # while not rospy.is_shutdown():
        pub1.publish(qvec[0])
        pub2.publish(qvec[1])
        pub3.publish(qvec[2])
        pub4.publish(qvec[3])
        pub5.publish(qvec[4])
        pub6.publish(qvec[5])
    
            

def listner():
    rospy.init_node('Robot_util_revice_Q')
    rospy.Subscriber("floats",Qvec,callback)
    rospy.spin()



if __name__=='__main__':
    try:
        listner()
    except rospy.ROSInterruptException:
        pass

